<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'post_id', 'parent_id', 'comment'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function replies()
    {
        return $this->hasMany('App\Comment', 'parent_id');
    }
}
