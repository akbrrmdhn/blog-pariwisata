<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use App\Category;
use App\Like;
use Auth;

class BlogController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index(){
        $posts = Post::all();
        
        return view('blog.index', compact("posts"));
    }

    public function create(){
        return view('blog.create');
    }

    public function store(Request $request){
        $request->validate([
            'post_title' => 'required|unique:posts',
            'post_description' => 'required'
        ]);
        $categories_arr = explode(',', $request["post_category"]);

        $categories_ids = [];
        foreach($categories_arr as $category){
            $cat = Category::firstOrCreate(['category' => $category]);
            $categories_ids[] = $cat->id;
            // if($cat){
            //     $categories_ids[] = $cat->id;
            // } else {
            //     $new_cat = Category::create(["category" => $category]);
            //     $categories_ids[] = $new_cat->id;
            // }
        }

        $post = Post::create([
            "post_title" => $request["post_title"],
            "post_description" => $request["post_description"],
            "user_id" => $request["userid"],
            "category_id" => $categories_ids[0]
        ]);
        
        // $post->categories()->sync($categories_ids);
        
        // $user->posts()->save($posts);

        return redirect('/blog')->with('success', 'Post berhasil disimpan!');
    }

    public function show($id){
        $post = Post::find($id);
        
        return view('blog.show', compact('post'));
    }

    public function edit($id){
        $post = Post::find($id);
        
        return view('blog.edit', compact('post'));
    }

    public function update($id, Request $request){
        // $request->validate([
        //     'post_title' => 'required|unique:posts',
        //     'post_description' => 'required'
        // ]);

        $update = Post::where('id', $id)->update([
            "post_title" => $request["post_title"],
            "post_description" => $request["post_description"]
        ]);

        return redirect('/blog')->with('success', 'Postingan berhasil diupdate!');
    }

    public function destroy($id){
        Post::destroy($id);
        return redirect('/blog')->with('success', 'Postingan berhasil dihapus!');
    }

    // public function postLikePost(Request $request)
    //    {
    //        $post_id = $request['post_id'];
    //        $is_like = $request['isLike'] === 'true';
    //        $update = false;
    //        $post = Post::find($post_id);
    //        if (!$post) {
    //            return null;
    //        }
    //        $user = Auth::user();
    //        $like = $user->likes()->where('post_id', $post_id)->first();
    //        if ($like) {
    //            $already_like = $like->like;
    //            $update = true;
    //            if ($already_like == $is_like) {
    //                $like->delete();
    //                return null;
    //            }
    //        } else {
    //            $like = new Like();
    //        }
    //        $like->like = $is_like;
    //        $like->user_id = $user->id;
    //        $like->post_id = $post->id;
    //        if ($update) {
    //            $like->update();
    //        } else {
    //            $like->save();
    //        }
    //        return null;
    //    }
}
