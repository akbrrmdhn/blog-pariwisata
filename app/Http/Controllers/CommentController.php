<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class CommentController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'comment'=>'required'
        ]);

        $input = $request->all();
        $input['user_id'] = Auth::user();

        Comment::create($input);

        return back();
    }
}
