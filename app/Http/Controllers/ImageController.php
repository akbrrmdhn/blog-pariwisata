<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function imageUploadPost(Request $request)
{
    request()->validate([
        'fileUpload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
   ]);
   if ($files = $request->file('fileUpload')) {
       $destinationPath = 'public/image/'; // upload path
       $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
       $files->move($destinationPath, $profileImage);
    }
    return back()
    ->withSuccess('Great! Image has been successfully uploaded.');
}
}
