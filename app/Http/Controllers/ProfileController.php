<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;

use PDF;

use App\Exports\ProfilesExport;
use Maatwebsite\Excel\Facades\Excel;

use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function __construct(){
      $this->middleware('auth')->except(['index', 'show']);
    } 
    
    public function export() 
    {
        return Excel::download(new ProfilesExport, 'DataPengguna.xlsx');
    }

    public function simpanpdf(){
      
      $profiles = Profile::all();

      $pdf = PDF::loadView('profiles.simpanpdf', compact("profiles"));
      
      return $pdf->download('profile.pdf');
    }

    public function upload(){
      return view('profiles.upload');
    }
  
    public function proses_upload(Request $request){

      $this->validate($request, [
        'file' => 'required',
      ]);
  
      // menyimpan data file yang diupload ke variabel $file
      $file = $request->file('file');
  
      //             // nama file
      // echo 'File Name: '.$file->getClientOriginalName();
      // echo '<br>';
  
      //             // ekstensi file
      // echo 'File Extension: '.$file->getClientOriginalExtension();
      // echo '<br>';
  
      //             // real path
      // echo 'File Real Path: '.$file->getRealPath();
      // echo '<br>';
  
      //             // ukuran file
      // echo 'File Size: '.$file->getSize();
      // echo '<br>';
  
      //             // tipe mime
      // echo 'File Mime Type: '.$file->getMimeType();
  
                  // isi dengan nama folder tempat kemana file diupload
      $tujuan_upload = 'foto';
  
                  // upload file
      $file->move($tujuan_upload,$file->getClientOriginalName());

      $namafile = $file->getClientOriginalName();

      return view('profiles.create', compact("namafile"));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $profiles = Profile::all();

        return view('profiles.index', compact("profiles"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $namafile = "";

        return view('profiles.create',compact("namafile"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //
      $request->validate([
          'nama_lengkap' => 'required',
          'tlp' => 'required',
          'foto' => 'required'
      ]);

      $id = Profile::where('user_id', '=',$request["userid"])->get();

      if(count($id) == 0)  {
        $profile = Profile::create([
          "fullname" => $request["nama_lengkap"],
          "phone" => $request["tlp"],
          "profpic" => "foto/".$request["foto"], 
          "user_id" => $request["userid"]
        ]);

        Alert::success('Berhasil', 'Data berhasil disimpan!');

        return redirect('/profiles')->with('success', 'Data berhasil disimpan!');

      }else {

        Alert::warning('Perhatian', 'Data Sudah Ada, Silahkan diubah!');

        return redirect('/profiles')->with('success', 'Data Sudah Ada, Silahkan diubah!');
      }
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $profile = DB::table('profiles')->where('id', $id)->first();

        $profile = Profile::find($id);
        
        return view('profiles.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $profile = DB::table('profiles')->where('id', $id)->first();

        $profile = Profile::find($id);
        
        return view('profiles.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $update = Profile::where('id', $id)->update([ "fullname" => $request["nama_lengkap"],
        "phone" => $request["tlp"],
        "profpic" => $request["foto"]
        ]);

        Alert::success('Berhasil', 'Data berhasil diupdate!');

        return redirect('/profiles')->with('success', 'Data berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Profile::destroy($id);

        Alert::success('Berhasil', 'Data berhasil dihapus!');

        return redirect('/profiles')->with('success', 'Data berhasil dihapus!');
    }
}
