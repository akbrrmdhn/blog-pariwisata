<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    //protected $primarykey = "idpost";
    protected $fillable = ["post_title", "post_description", "user_id", "category_id"];
    //protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }
    public function categories(){
        return $this->belongsToMany('App\Category', 'post_category', 'post_id', 'category_id');
    }

    public function likes(){
        return $this->hasMany('App\Likes');
    }
}
