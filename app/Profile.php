<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $table = "profiles";

    protected $fillable = ["fullname","phone","profpic", "user_id"];

    protected $guarded = [];

    public function user(){
      return $this->belongTo('App\User');
    }
}
