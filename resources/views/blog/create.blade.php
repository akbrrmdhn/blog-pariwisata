@extends('layouts.master')

@section('content')
<div class="ml-4 mr-4 mt-3">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">New Blog Post</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="/blog" method="POST">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="post_title">Title</label>
          <input type="text" class="form-control" id="post_title" value="{{ old('post_title', '')}}" name="post_title" placeholder="Title..." required>
          @error('post_title')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          </div>
          <div class="form-group">
            <label for="post_description">Description</label>
            <input type="text" class="form-control" id="post_description" name="post_description" value="{{ old('post_description', '')}}" placeholder="Write a description..." required>
          </div>
          @error('post_description')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <div class="form-group">
            <label for="post_category">Categories</label>
              <input type="text" class="form-control" id="post_category" name="post_category" value="{{ old('post_category', '')}}" placeholder="Write tags, separated by comma" >
          </div>
          {{-- <form action="{{ route('image.upload.post') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">

            <div class="col-md-6">
                <input type="file" name="image" class="form-control">
            </div>

            <div class="col-md-6">
                <button type="submit" class="btn btn-success">Upload</button>
            </div>

          </div>
        </form> --}}
          <div class="form-group">
            <input type="hidden" class="form-control" id="userid" name="userid" value="{{ Auth::user()->id}}" placeholder="Userid" required>
          </div>
        </div>
        <!-- /.card-body -->
  
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Post it!</button>
        </div>
      </form>
    </div>
  
  </div>
@endsection