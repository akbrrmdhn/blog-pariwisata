@extends('layouts.master')

@section('content')
<div class="ml-4 mr-4 mt-3">
    <div class="card card-primary">
      <div class="card-header">
      <h3 class="card-title">Edit Blog Post {{$post->id}}</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="/blog/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
          <div class="form-group">
            <label for="post_title">Title</label>
          <input type="text" class="form-control" id="post_title" value="{{ old('post_title', $post->post_title)}}" name="post_title" placeholder="Title..." required>
          @error('post_title')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          </div>
          <div class="form-group">
            <label for="post_description">Description</label>
            <input type="text" class="form-control" id="post_description" name="post_description" value="{{ old('post_description', $post->post_description)}}" placeholder="Write a description..." required>
          </div>
          @error('post_description')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          
        </div>
        <!-- /.card-body -->
  
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Post it!</button>
        </div>
      </form>
    </div>
  
  </div>
@endsection