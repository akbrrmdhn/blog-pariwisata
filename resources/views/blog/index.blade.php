@extends('layouts.master')

@section('content')
<div>
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Postingan Blog Pariwisata</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
          @endif
          <a button class="btn btn-primary" href="{{ route('blog.create')}}">Create New Blog Post</a>
          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Post Title</th>
                <th>Description</th>
                {{-- <th>Like/Dislike</th> --}}
                <th style="width: 40px">Action(s)</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($posts as $key => $postingan)
                <tr>
                <td>{{$key + 1}}</td>
                <td>{{$postingan->post_title}}</td>
                <td>{{$postingan->post_description}}</td>
                {{-- <td> 
                  <div class="interaction"> 
                    <a href="#" class="btn btn-xs btn-warning like">{{ Auth::user()->likes()->where('post_id', $post->id)->first() ? Auth::user()->likes()->where('post_id', $post->id)->first()->like == 1 ? `You like this post` : `Like` : `Like`  }}</a> |
                    <a href="#" class="btn btn-xs btn-danger like">{{ Auth::user()->likes()->where('post_id', $post->id)->first() ? Auth::user()->likes()->where('post_id', $post->id)->first()->like == 0 ? `You don't like this post` : `Dislike` : `Dislike`  }}</a></div> 
                </td>
                 --}}
                <td style="display: flex">
                <a href="{{ route('blog.show', ['blog' => $postingan->id])}}" class="btn btn-info btn-sm">Show</a>
                <a href="/blog/{{$postingan->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                <form action="/blog/{{$postingan->id}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
                
                </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" align="center">No Posts</td>
                </tr>
            @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{--<div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div>--}}
      </div>
</div>
@endsection

<script src="{{asset('/js/like.js')}}"></script>