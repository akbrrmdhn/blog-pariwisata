@extends('layouts.master')

@section('content')
<div class="ml-4 mr-4 mt-3">
    <div class="card card-primary">
        <div class="mt-3 ml-3">
            <h2>{{$post->post_title}}</h2>
            <p> Author: {{$post->user->profile->fullname}}</p>
            <p>{{$post->post_description}}</p>
            {{-- <h4>Display Comments</h4>
  
                    @include('blog.display', ['comments' => $post->comments, 'post_id' => $post->id])
   
                    <hr />
                    <h4>Add comment</h4>
                    <form method="post" action="{{ route('comment.store'   ) }}">
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" name="body"></textarea>
                            <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Add Comment" />
                        </div>
                    </form> --}}
            {{-- <div>
                Tags:
                @forelse ($post->categories as $cat)
                    <button class="btn btn-primary btn-sm">{{ $cat}}</button>
                @empty
                    No tags
                @endforelse
            </div> --}}
        </div>
    </div>
  </div>
@endsection