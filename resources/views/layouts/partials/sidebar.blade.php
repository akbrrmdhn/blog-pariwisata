<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('/AdminLTE/dist/img/AdminLTELogo.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">NewNormal Destination</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('/AdminLTE/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @guest
            <a href="#" class="d-block">Guest</a>
          @else

          {{Auth::user()->profile}}

          @if (Auth::user()->profile === null)
          <a href="{{ route('profiles.index')}}"
            class="d-block"> {{ Auth::user()->name}}</a>
          @else
           <a href="{{ route('profiles.show', Auth::user()->profile->id )}}"
               class="d-block"> {{ Auth::user()->profile->fullname }}</a>
          @endif

          @error('username')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror

          @endguest
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
          <li class="nav-header">MENU</li>
          <li class="nav-item">
            <a href="{{route('blog.index')}}" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Blog</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('profiles.index')}}" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Profile</p>
            </a>
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>