@extends('layouts.master')

@section('content')
<div class="ml-4 mr-4 mt-3">

    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Data Pribadi</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="/profiles" method="POST">
        @csrf
        <div class="card-body">

          <label for="foto">Upload Foto</label>

          <a href="{{ route('profiles.upload')}}" class="btn btn-primary btn-sm" id="foto">Upload</a>

          <div class="form-group">
            <label for="foto">Folder Foto</label>
            <input type="text" class="form-control" id="foto" name="foto" value="{{ old('foto',$namafile)}}" placeholder="Foto" required>
          </div>

          <div class="form-group">
            <label for="nama_lengkap">Nama Lengkap</label>
          <input type="text" class="form-control" id="nama_lengkap" value="{{ old('nama_lengkap', '')}}" name="nama_lengkap" placeholder="Nama Lengkap" required>
          @error('nama_lengkap')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          </div>
          <div class="form-group">
            <label for="tlp">No. Telepon</label>
            <input type="text" class="form-control" id="tlp" name="tlp" value="{{ old('tlp', '')}}" placeholder="No. Telepon" required>
          </div>
          @error('tlp')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          
         
          {{-- <div class="form-group">
            <label for="foto">Foto</label>
            <input type="text" class="form-control" id="foto" name="foto" value="{{ old('foto', '')}}" placeholder="Foto" required>
          </div>
          @error('foto')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror --}}

          <div class="form-group">
            <input type="hidden" class="form-control" id="userid" name="userid" value="{{ Auth::user()->id}}" placeholder="Userid" required>
          </div>
          
        </div>
        <!-- /.card-body -->
  
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">SIMPAN</button>
        </div>
      </form>
    </div>
  
  </div>
@endsection

@push('scripts')

<script>

</script>

@endpush