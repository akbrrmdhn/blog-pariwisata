@extends('layouts.master')

@section('content')
<div>
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Daftar Pengguna</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

          @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
          @endif

          <a button class="btn btn-primary" href="{{ route('profiles.create')}}">Tambah Data Pribadi</a>

          <a button class="btn btn-primary" href="{{ route('profiles.simpanpdf')}}">Simpan PDF</a>

          <a button class="btn btn-primary" href="{{ route('profiles.export')}}">EXPORT EXCEL</a>

          <br> </br>

          <table class="table table-bordered">
            <thead>                  
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama Lengkap</th>
                <th>No. Telepon</th>
                <th>Folder Gambar</th>
                <th style="width: 40px">Action(s)</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($profiles as $key => $profile)
                <tr>
                <td>{{$key + 1}}</td>
                <td>{{$profile->fullname}}</td>
                <td>{{$profile->phone}}</td>
                <td>{{$profile->profpic}}</td>
                <td style="display: flex">

                <a href="{{ route('profiles.show', ['profile' => $profile->id])}}" class="btn btn-info btn-sm">Show</a>

                <a href="/profiles/{{$profile->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                <form action="/profiles/{{$profile->id}}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
                
                </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" align="center">Tidak Ada Data</td>
                </tr>
            @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{--<div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div>--}}
      </div>
</div>
@endsection