@extends('layouts.master')

@section('content')

<img class="img-fluid mb-3" src="{{ asset($profile->profpic)}}" alt="Photo">

<div class="ml-4 mr-4 mt-3">
    <div class="card card-primary">
        <div class="mt-3 ml-3">
            <h4>NAMA LENGKAP</h4>
            <p>{{$profile->fullname}}</p>
            <h5>No. Telepon</h5>
            <p>{{$profile->phone}}</p>
        </div>
    </div>
  </div>
@endsection