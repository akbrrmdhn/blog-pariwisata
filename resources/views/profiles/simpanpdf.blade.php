<!DOCTYPE html>
<html>
<head>
	<title>Laporan Daftar Pengguna</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Daftar Pengguna</h4>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
        <th style="width: 10px">#</th>
        <th>Nama Lengkap</th>
        <th>No. Telepon</th>
        <th>Folder Gambar</th>
			</tr>
		</thead>
		<tbody>

      @forelse ($profiles as $key => $profile)
      <tr>
      <td>{{$key + 1}}</td>
      <td>{{$profile->fullname}}</td>
      <td>{{$profile->phone}}</td>
      <td>{{$profile->profpic}}</td>

      @empty
      <tr>
          <td colspan="4" align="center">Tidak Ada Data</td>
      </tr>

      @endforelse
		</tbody>
	</table>

</body>
</html>