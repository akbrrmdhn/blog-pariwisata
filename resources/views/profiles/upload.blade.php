@extends('layouts.master')

@section('content')

  <div class="card card-primary">

    <div class="card-header">
      <h3 class="card-title">UPLOAD FILE</h3>
    </div>

    <div class="card-body">

      @if(count($errors) > 0)
      <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }} <br/>
        @endforeach
      </div>
      @endif

      <form action="{{route('profiles.proses_upload')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group">
          <b>File Foto</b><br/>
          <input type="file" name="file">
        </div>

        {{-- <div class="form-group">
          <b>Keterangan</b>
          <textarea class="form-control" name="keterangan"></textarea>
        </div> --}}

        <input type="submit" value="Upload" class="btn btn-primary">
      </form>

    </div>
    
  </div>
  

@endsection