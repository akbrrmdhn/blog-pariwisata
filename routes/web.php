<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {    return view('welcome');});
// Route::get('/blog', 'BlogController@index');
// Route::get('/blog/create', 'BlogController@create');
// Route::post('/blog', 'BlogController@store');
// Route::get('/blog/{id}', 'BlogController@show');
// Route::get('/blog/{blog_id}/edit', 'BlogController@edit');
// Route::put('/blog/{blog_id}', 'BlogController@update');
// Route::delete('/blog/{blog_id}', 'BlogController@destroy');

Route::resource('blog', 'BlogController');
Route::post('/like','BlogController@likePost')->name('like');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profiles/simpanpdf', 'ProfileController@simpanpdf')->name('profiles.simpanpdf');

Route::get('/profiles/upload', 'ProfileController@upload')->name('profiles.upload');

Route::post('/profiles/proses_upload', 'ProfileController@proses_upload')->name('profiles.proses_upload');

Route::get('profiles/export/', 'ProfileController@export')->name('profiles.export');

Route::resource('profiles', 'ProfileController');

Route::get('/upload', 'UploadController@upload');
Route::post('/upload/proses', 'UploadController@proses_upload');

